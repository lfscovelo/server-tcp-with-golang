#!/bin/bash

# Força a parada do container e remove
docker rm -f server-tcp

git clone git@gitlab.com:lfscovelo/server-tcp-with-golang.git

cd server-tcp-with-golang

docker-compose up -d --build
