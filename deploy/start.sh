#!/bin/bash
set -e

# Install and start SSH
command -v ssh-agent >/dev/null || ( apt-get update -y && apt-get install openssh-client -y )
eval $(ssh-agent -s)

# Create SSH dir with correct permissions
mkdir -p ~/.ssh
chmod 700 ~/.ssh

# Add private key to the SSH agent
echo -e "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ssh-add ~/.ssh/id_rsa

# Add ssh known hosts with correct permissions
ssh-keyscan -t rsa $HOST >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

ssh $USER@$HOST < ./deploy/deploy.sh