package main

import (
	"encoding/hex"
	"fmt"
	"log"
	"net"
)

func main() {

	listener, err := net.Listen("tcp", ":9009")
	if err != nil {
		log.Printf("Unable to start server on :9009: %s", err.Error())
	}

	defer listener.Close()
	log.Printf("started server on :9009")

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Unable to accept connection: %s", err.Error())
			continue
		}

		go handle(conn)
	}

}

func handle(conn net.Conn) {
	fmt.Printf("handle new client: %s", conn.RemoteAddr().String())

	for {
		buf := make([]byte, 1024)
		n, err := conn.Read(buf)
		if err != nil {
			return
		}
		if n == 0 {
			return
		}
		data := hex.EncodeToString(buf[:n])
		log.Printf("command received: %s", data)

		conn.Write([]byte("FE"))
	}
}
